package bigdata;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TfIdfTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TfIdfTest(String testName)
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( TfIdfTest.class );
    }

    public void test_getTermFrequency_returns_correct_result(){
        TfIdf tfIdf = new TfIdf();
        List<String> testTerms = Arrays.asList(new String[]{"test1", "test2"});
        assertEquals( 0.5, tfIdf.getTermFrequency(testTerms, "test1") );
    }

    public void test_getInverseDocumentFrequency_returns_correct_result(){
        Map<String, List<String>> documentsTerms = new HashMap<String, List<String>>();
        documentsTerms.put("doc1", Arrays.asList(new String[]{"test1", "test2"}));
        documentsTerms.put("doc2", Arrays.asList(new String[]{"test1", "test2", "test3"}));
        TfIdf tfIdf = new TfIdf();
        assertEquals( 1.6931471805599454, tfIdf.getInverseDocumentFrequency(documentsTerms, "test3") );
    }
}
