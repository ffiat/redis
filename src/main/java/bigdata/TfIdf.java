package bigdata;
/**
 * Created by FiatF on 11/22/2015.
 */

import java.util.List;
import java.util.Map;

public class TfIdf {

    public double getTermFrequency(List<String> documentTerms, String termToCheck) {
        double count = 0;
        for (String s : documentTerms) {
            if (s.equalsIgnoreCase(termToCheck)) {
                count++;
            }
        }
        return count / documentTerms.size();
    }

    public double getInverseDocumentFrequency(Map<String, List<String>> documentsTerms, String termToCheck) {
        double count = 0;
        for (List<String> documentTerms : documentsTerms.values()) {
            for (String documentTerm : documentTerms) {
                if (documentTerm.equalsIgnoreCase(termToCheck)) {
                    count++;
                    break;
                }
            }
        }
        return 1 + Math.log(documentsTerms.size() / count);
    }

}
