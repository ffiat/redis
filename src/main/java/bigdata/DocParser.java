package bigdata;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * Created by FiatF on 11/22/2015.
 */
public class DocParser {

    final static Logger logger = Logger.getLogger(DocParser.class);

    private Map<String, List<String>> documentsTerms =  new HashMap<String, List<String>>();

    private Set<String> allTerms = new HashSet<String>();

    public Set<String> getAllTerms() {
        return allTerms;
    }

    public Map<String, List<String>> getDocumentsTerms() {
        return documentsTerms;
    }

    public void parse(File dataSource){
        for(File file : getFiles(dataSource)){
            List<String> terms = parseFile(file);
            documentsTerms.put(file.getAbsolutePath(), terms);
            allTerms.addAll(terms);
        }
    }

    private List<File> getFiles(File dataSource){
        List<File> files = new ArrayList<File>();
        if(dataSource.isDirectory()){
            for (File file : dataSource.listFiles()){
                if(file.isDirectory()){
                    files.addAll(getFiles(file));
                }else if(file.getName().toLowerCase().endsWith(".txt")){
                    files.add(file);
                }
            }
        }
        return files;
    }

    private List<String> parseFile(File file){
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            StringBuilder sb = new StringBuilder();
            String s = null;
            while ((s = in.readLine()) != null) {
                sb.append(s);
            }
            String[] terms = sb.toString().toLowerCase().replaceAll("[\\W&&[^\\s]]", "").split("\\W+");

            return Arrays.asList(terms);
        } catch (FileNotFoundException fnfEx) {
            logger.error("Unable to find file" + file.getAbsolutePath(), fnfEx);
        } catch (IOException ioEx) {
            logger.error("Error during processing file" + file.getAbsolutePath(), ioEx);
        }
        return new ArrayList<String>();
    }
}
