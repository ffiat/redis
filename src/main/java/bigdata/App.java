package bigdata;

import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.io.File;

/**
 * Created by FiatF on 11/22/2015.
 */
public class App {

    final static Logger logger = Logger.getLogger(App.class);

    private DocParser docParser = new DocParser();
    private TfIdf tfIdfCalculator = new TfIdf();

    //private Jedis jedis = new Jedis("redis://filipfiat:7aa416f777419c4c415529dadaf66441@50.30.35.9:3348/");

    private Jedis jedis = new Jedis("redis://redistogo:d87694819e4b5f9a79dd05ac54e78870@panga.redistogo.com:9969/");

    public DocParser getDocParser() {
        return docParser;
    }

    public TfIdf getTfIdfCalculator() {
        return tfIdfCalculator;
    }

    public Jedis getJedis(){
        return jedis;
    }

    public static void main(String[] args){
        App app = new App();
        String dataSource = "resources";
        DocParser parser = app.getDocParser();
        TfIdf tfIdfCalculator = app.getTfIdfCalculator();
        parser.parse(new File(dataSource));
//        app.getJedis().set("foo", "bar");
//        System.out.println(app.getJedis().get("foo"));
        Transaction transaction = app.getJedis().multi();
        for(String document: parser.getDocumentsTerms().keySet()){
            for(String term: parser.getAllTerms()){
                double tf = tfIdfCalculator.getTermFrequency(parser.getDocumentsTerms().get(document), term);
                double idf = tfIdfCalculator.getInverseDocumentFrequency(parser.getDocumentsTerms(), term);
                double tfIdf = tf * idf;
                if(tfIdf > 0) {
                    logger.info("Adding term " + term + " from document " + document + " with TF-IDF " + tfIdf);
                    transaction.sadd(document, term, String.valueOf(tfIdf));
                }
            }
        }
        transaction.exec();

    }
}
